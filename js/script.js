function createNewUser() {
    let userName = prompt("Youre name: ");
    let userSurname = prompt("Youre surname: ");

    
    const newUser = {
        firstName: userName,
        lastName: userSurname,
        birthday: null,
        birthdayDate: null,
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        setFirstName(newName) {
            this.firstName = newName;
        },
        setLastName(newName) {
            this.lastName = newName;
        },
        insertBirthday(){
            birthdayUser = prompt("Youre birthday(dd.mm.yyyy)");
            birthdayArr = birthdayUser.split('.');
            day = parseInt(birthdayArr[0]);
            month = parseInt(birthdayArr[1]) -1;
            year = parseInt(birthdayArr[2]);
            this.birthdayDate = new Date(year, month, day);
        },
        getAge(){
            timeNow = Date.now();
            timeDiff = Math.abs(timeNow - this.birthdayDate.getTime());
            return Math.floor(timeDiff / (1000 * 3600 * 24 * 365.25));
        },
        getPassword(){
            return this.getLogin() + this.birthdayDate.getFullYear();
        }
    };
    return newUser;
}
const user = createNewUser();
console.log(user.getLogin());
user.setFirstName("anotherName");
console.log(user.getLogin()); 
user.insertBirthday();
console.log(user.getAge());
console.log(user.getPassword());